import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThecocktaildbViewComponent } from './thecocktaildb-view.component';

describe('ThecocktaildbViewComponent', () => {
  let component: ThecocktaildbViewComponent;
  let fixture: ComponentFixture<ThecocktaildbViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThecocktaildbViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThecocktaildbViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
