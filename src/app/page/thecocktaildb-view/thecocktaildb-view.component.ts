import {Component, OnInit} from '@angular/core';
import {ProductModel} from '../../shared/interfaces/product.model';
import {ActivatedRoute} from '@angular/router';
import {ThecocktaildbService} from '../../shared/services/thecocktaildb/thecocktaildb.service';

@Component({
  selector: 'mp-thecocktaildb-view',
  templateUrl: './thecocktaildb-view.component.html',
  styleUrls: ['./thecocktaildb-view.component.scss']
})
export class ThecocktaildbViewComponent implements OnInit {
  public idCategory: string;
  public idDrink: string;
  public drink: ProductModel;

  public ingredients = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

  constructor(private route: ActivatedRoute,
              private thecocktaildbService: ThecocktaildbService) {
    this.idCategory = route.snapshot.params['cat'];
    this.idDrink = route.snapshot.params['id'];
  }

  ngOnInit() {
    this.getById();
  }

  getById() {
    if (this.idCategory && this.idDrink) {
      this.thecocktaildbService.getById(this.idCategory, this.idDrink).subscribe(resp => {
        this.drink = resp && resp['drinks'] && resp.drinks.length ? resp.drinks[0] : null;
      });
    }
  }

}
