import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {PageComponent} from './page.component';
import {ThecocktaildbListComponent} from './thecocktaildb-list/thecocktaildb-list.component';
import {ThecocktaildbViewComponent} from './thecocktaildb-view/thecocktaildb-view.component';

export const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    children: [
      {path: '', redirectTo: 'list', pathMatch: 'full'},
      {
        path: 'list',
        component: ThecocktaildbListComponent,
        data: {breadcrumb: 'Lista'}
      },
      {
        path: 'view/:cat/:id',
        component: ThecocktaildbViewComponent,
        data: {breadcrumb: 'Ver'}
      }
    ]
  }
];

export const pageRouting: ModuleWithProviders = RouterModule.forChild(routes);
