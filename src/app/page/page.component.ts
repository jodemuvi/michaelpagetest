import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'mp-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  onHome() {
    this.router.navigate(['page/list']);
  }
}
