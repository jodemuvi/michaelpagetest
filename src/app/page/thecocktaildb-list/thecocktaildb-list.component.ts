import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ThecocktaildbService} from '../../shared/services/thecocktaildb/thecocktaildb.service';
import {DrinkModel} from '../../shared/interfaces/drink.model';
import {ProductModel} from '../../shared/interfaces/product.model';

@Component({
  selector: 'mp-thecocktaildb-list',
  templateUrl: './thecocktaildb-list.component.html',
  styleUrls: ['./thecocktaildb-list.component.scss']
})
export class ThecocktaildbListComponent implements OnInit {
  public filterForm: FormGroup;
  public drinks: Array<DrinkModel> = [];
  public products: Array<ProductModel> = [];

  public drinksAutocomplete: Array<DrinkModel> = [];
  config: any = {max: 5, placeholder: 'Autocomplete', sourceField: ['strAlcoholic']};

  constructor(private router: Router,
              private thecocktaildbService: ThecocktaildbService) {
  }

  ngOnInit() {
    this.onSelectedCriteria('c');
    this.cretaeForm();
  }

  private cretaeForm() {
    this.filterForm = new FormGroup({
      criteria: new FormControl('c', Validators.required),
      drink: new FormControl(null, Validators.required)
    });
  }

  onSelectedCriteria(event) {
    this.thecocktaildbService.findByCriteria(event).subscribe(resp => {
      this.drinks = resp['drinks'] ? resp['drinks'] : [];
    });
  }

  onSelectedDrink(event) {
    this.thecocktaildbService.findByCriteriaAndDrink(this.filterForm.value['criteria'], event).subscribe(resp => {
      this.products = resp['drinks'] ? resp['drinks'] : [];
    });
  }

  onAutocompleteSearch(event: string) {
    this.filterForm.get('drink').setValue(event);
    event = (event ? event : '').toLowerCase();
    this.drinksAutocomplete = this.drinks.filter(value => ((value.strAlcoholic ? value.strAlcoholic : '').toLowerCase().search(event) !== -1));
  }

  onAutocompleteSelected(event) {
    if (event['strAlcoholic']) {
      this.onSelectedDrink(event['strAlcoholic']);
    }
  }

  onDetail(_id: string) {
    this.router.navigate(['page/view/' + this.filterForm.value['criteria'] + '/' + _id]);
  }
}
