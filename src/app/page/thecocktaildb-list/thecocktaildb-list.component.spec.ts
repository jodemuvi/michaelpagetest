import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThecocktaildbListComponent } from './thecocktaildb-list.component';

describe('ThecocktaildbListComponent', () => {
  let component: ThecocktaildbListComponent;
  let fixture: ComponentFixture<ThecocktaildbListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThecocktaildbListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThecocktaildbListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
