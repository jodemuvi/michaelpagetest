import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageComponent} from './page.component';
import {ThecocktaildbListComponent} from './thecocktaildb-list/thecocktaildb-list.component';
import {ThecocktaildbViewComponent} from './thecocktaildb-view/thecocktaildb-view.component';
import {pageRouting} from './page.routing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AutocompleteModule} from '../shared/global/autocomplete/autocomplete.module';

@NgModule({
  imports: [
    CommonModule,
    pageRouting,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteModule.forRoot()
  ],
  declarations: [PageComponent, ThecocktaildbListComponent, ThecocktaildbViewComponent]
})
export class PageModule {
}
