import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

/**
 * Injectable service
 * @export
 */
@Injectable()
export class LoadingSpinnerService {

    /**
     * @description spinners BehaviorSubject
     * @memberof LoadingSpinnerService
     */
    public spinnerSubject: BehaviorSubject<any> = new BehaviorSubject<any>(false);

    /**
     * Creates an instance of LoadingSpinnerService.
     * @memberof LoadingSpinnerService
     */
    constructor() {

    }

    /**
     * To show spinner
     * @memberof LoadingSpinnerService
     */
    show() {
        this.spinnerSubject.next(true);
    }

    /**
     * To hide spinner
     * @memberof LoadingSpinnerService
     */
    hide() {
        this.spinnerSubject.next(false);
    }

    getMessage(): Observable<any> {
        return this.spinnerSubject.asObservable();
    }
}
