export interface DrinkModel {
  strCategory: string;
  strGlass: string;
  strIngredient1: string;
  strAlcoholic: string;
}
