import {Injectable} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class SessionService {

  public static STRING_TOKEN = 'access_token';

  constructor() {
  }

  exists(key) {
    return localStorage.getItem(key);
  }

  setItem(key, value) {
    localStorage.setItem(key, value);
  }

  public getItem(key): any {
    return localStorage.getItem(key);
  }

  setObject(key, object) {
    const value = JSON.stringify(object);
    localStorage.setItem(key, value);
  }

  getObject(key) {
    const value = localStorage.getItem(key);
    return JSON.parse(value);
  }

  clear() {
    localStorage.clear();
  }

  getHeaderOauth(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getItem(SessionService.STRING_TOKEN)
    });
  }

  getHeader(): HttpHeaders {
    return new HttpHeaders({});
  }

  getHeaderMultipartOauth(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.getItem(SessionService.STRING_TOKEN)
    });
  }

  getHeaderMultipart(): HttpHeaders {
    return new HttpHeaders({
      /*'Content-Type': 'multipart/form-data',*/
    });
  }

}
