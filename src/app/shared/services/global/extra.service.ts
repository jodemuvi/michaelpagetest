import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {finalize, catchError} from 'rxjs/operators';
import {SessionService} from './session.service';
import {LoadingSpinnerService} from '../../global/loading-spinner';

@Injectable({providedIn: 'root'})
export class ExtraService {

  constructor(private http?: HttpClient,
              private sessionService?: SessionService,
              private spinnerService?: LoadingSpinnerService) {
  }

  public getAnyService(_url: string, _params: HttpParams = null, _oauth: boolean = true, _showLoader: boolean = true): Observable<any> {
    this.spinnerShow(_showLoader);
    return this.http.get(_url, {headers: this.headerGlobal(_oauth), params: _params})
      .pipe(finalize(() => {
        this.spinnerHide(_showLoader);
      }), catchError((err) => {
        return throwError(err);
      }));
  }

  public postAnyService(_url: string, _requestBody: any, _params: HttpParams, _oauth: boolean, _showLoader: boolean = true): Observable<any> {
    this.spinnerShow(_showLoader);
    return this.http.post(_url, _requestBody, {headers: this.headerGlobal(_oauth), params: _params})
      .pipe(finalize(() => {
        this.spinnerHide(_showLoader);
      }), catchError((err) => {
        return throwError(err);
      }));
  }

  public deleteAnyService(_url: string, _params: HttpParams, _oauth: boolean, _showLoader: boolean = true): Observable<any> {
    this.spinnerShow(_showLoader);
    return this.http.delete(_url, {headers: this.headerGlobal(_oauth), params: _params})
      .pipe(finalize(() => {
        this.spinnerHide(_showLoader);
      }), catchError((err) => {
        return throwError(err);
      }));
  }

  public postMultipartService(_url: string, _requestBody: any, _params: HttpParams, _oauth: boolean, _showLoader: boolean = true): Observable<any> {
    this.spinnerShow(_showLoader);
    return this.http.post(_url, _requestBody, {headers: this.headerGlobal(_oauth, true), params: _params})
      .pipe(finalize(() => {
        this.spinnerHide(_showLoader);
      }), catchError((err) => {
        return throwError(err);
      }));
  }

  //#region METHODS
  private spinnerShow(showLoader: boolean) {
    if (showLoader) {
      this.spinnerService.show();
    }
  }

  private spinnerHide(showLoader: boolean) {
    if (showLoader) {
      this.spinnerService.hide();
    }
  }

  private headerGlobal(oauth: boolean, multipart: boolean = false): HttpHeaders {
    if (multipart) {
      if (oauth) {
        return this.sessionService.getHeaderMultipartOauth();
      } else {
        return this.sessionService.getHeaderMultipart();
      }
    } else {
      if (oauth) {
        return this.sessionService.getHeaderOauth();
      } else {
        return this.sessionService.getHeader();
      }
    }
  }

  //#endregion
}
