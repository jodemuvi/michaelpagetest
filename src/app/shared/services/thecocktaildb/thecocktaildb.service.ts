import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {ExtraService} from '../global/extra.service';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class ThecocktaildbService {
  private urlBase = environment.services + '/';

  private list = this.urlBase + 'api/json/v1/1/list.php';
  private search = this.urlBase + 'api/json/v1/1/filter.php';
  private info = this.urlBase + 'api/json/v1/1/lookup.php';

  constructor(private extraService: ExtraService) {
  }

  findByCriteria(_value: string): Observable<any> {
    const params = new HttpParams()
      .set(_value, 'list');
    return this.extraService.getAnyService(this.list, params, false);
  }

  findByCriteriaAndDrink(_value: string, _filter: string): Observable<any> {
    const params = new HttpParams()
      .set(_value, _filter);
    return this.extraService.getAnyService(this.search, params, false);
  }

  getById(_value: string, _id: string): Observable<any> {
    const params = new HttpParams()
      .set(_value, _id);
    return this.extraService.getAnyService(this.info, params, false);
  }
}
