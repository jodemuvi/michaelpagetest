import { TestBed } from '@angular/core/testing';

import { ThecocktaildbService } from './thecocktaildb.service';

describe('ThecocktaildbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ThecocktaildbService = TestBed.get(ThecocktaildbService);
    expect(service).toBeTruthy();
  });
});
