import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {NotFoundComponent} from './components/not-found/not-found.component';

export const routes: Routes = [
  {path: '', redirectTo: 'page', pathMatch: 'full'},
  {
    path: 'page',
    loadChildren: './page/page.module#PageModule',
  },
  {path: '**', redirectTo: 'not-found'},
  {path: 'not-found', component: NotFoundComponent},
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
  // preloadingStrategy: PreloadAllModules,
  useHash: true
});
