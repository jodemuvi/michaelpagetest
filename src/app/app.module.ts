import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {LoadingSpinnerModule} from './shared/global/loading-spinner';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {routing} from './app.routing';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    routing,
    LoadingSpinnerModule.forRoot()
  ],
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  providers: [
    NotFoundComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
