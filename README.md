## MICHAELPAGE
Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

#### Crear el proyecto
Ejecuté `ng new michaelpage --prefix=mp --style=scss` para generar un proyecto con prefijo `mp` y hojas de estilo `scss`

#### Servidor de desarrollo
Ejecuté `ng serve` para un servidor de desarrollo. Navegue a `http://localhost:4200/`. La aplicación se volverá a cargar automáticamente si cambia alguno de los archivos de origen.

#### Code scaffolding
Ejecuté `ng generate component component-name` para generar un nuevo componente. También puede usar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

#### Compilar
Ejecuté `ng build` para compilar el proyecto. Los artefactos de compilación se almacenarán en el directorio `dist/`. Use el indicador `--prod` para una compilación de producción.

#### Ejecución de pruebas unitarias
Ejecuté `ng test` para ejecutar las pruebas unitarias a través de [Karma](https://karma-runner.github.io).

#### Ejecución de pruebas de end-to-end
Ejecuté `ng e2e` para ejecutar las pruebas de end-to-end a través de [Protractor](http://www.protractortest.org/).

#### Ayuda adicional
Para obtener más ayuda sobre la CLI Angular, use `ng help` o visite el [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## DESARROLLO
#### Instalación de Bootstrap v4.x, JQuery y Popper.js
- Para el buen funcionamiento de bootstrap se requiere la instalación de jquery y popper.js. Ejecutamos el comando de `npm install bootstrap@latest jquery popper.js --save`
- Agregar en `angular.json` lo siguiente: <br/> ![](src/assets/imgs/config-bootstrap-jquery-popperjs.jpg)
- Pará validar la integración de bootstrap, jquery y popper.js crearemos un ejemplo en `app.component.html`.<br/> ![](src/assets/imgs/validate-bootstrap-jquery-popperjs.jpg)

#### Estructura básica del proyecto
- creamos 4 paquetes `components, page, shared, assets` para lo siguiente: <br/> ![](src/assets/imgs/proyecto-estructura-base.jpg)
    - `1: components:` Componentes generícos que pueden ser utilizados por todo el proyecto.
    - `2: page:` Módulos y componentes (como páginas) que necesita el proyecto.
    - `3: shared:` Recursos compartidos para accede en todo el proyecto como: `Controls, directives, pipes, models (class), interfaces, enums, interceptors, guards, services` y otros si es necesario.
    - `4: assets:` Recursos públicos como `images, styles, fonts, js` y otros que se necesite de acceso de público.

#### Recursos utilizados para el test
- `shared:` los recursos utilizados en shared fueron: <br/> ![](src/assets/imgs/proyecto-estructura-shared.jpg)
    - `1: Global:` en global se creó dos componentes: `autocomplete` para el control de autocomplete de un input y `loading-spinner` para mostrar la espera de una solicitud realizada.
    - `2: interfaces:` se creó 2 modelos para mapear los valores que llega en la respuesa de un request.
    - `3: services:` se creó `session.service.ts` para la gestión de sesiones en caso existiera, `extra.service.ts` para la personalización global de una petición GET, POST, etc. y `thecocktaildb.service.ts` para los servicios que se consumirá.
- `components y page:` los recursos utilizados fueron: <br/> ![](src/assets/imgs/proyecto-estructura-componets-page.jpg)
    - `1: components -> not-found:` se creó como página para la navegación de rutas no existentes en el proyecto. urls no encontradas.
    - `2: page -> page.module.ts y page.routing.ts` se creó para la gestión de enrutamiento de las páginas.
    - `3: page -> page.component.[html,ts,scss]` se creó como página padre que contiene el header y cargarán las páginas hijos (childs).
    - `4: page -> thecocktaildb-list:` se creó para la página de los filtros de las bebidas. <br/> ![](src/assets/imgs/capture-categories.jpg)
    - `5: page -> thecocktaildb-view:` se creó para la página de la información de un producto seleccionado. <br/> ![](src/assets/imgs/capture-info-drink.jpg)

#### Sobre pruebas unitarias (.spec.ts)
se debe ejeuctar el comando `ng test --code-coverage`. adicional si necesitas pasar a sonarqube debes ejecutar `npm install -g sonarqube-scanner` y `npm run sonar`

### Google Lighthouse
Se ejecuto en localhost con plugin en chrome <br/> ![](src/assets/imgs/capture-google-lighthouse.jpg)
- `Accessibility:` para mejorar debemos estructurar adecuadamente la contenido visual, utilizar etiquetas html y estilos no deprecados, aplicar las buenas practicas, definir colores no opacos, etc.
- `SEO:` Agregar en las páginas los metadatos, tags, adicional podemos implementar Google Analitycs, Firebase, sitemaps, robots, etc. 

#### Capturas de la funcionalidad
- Criterio -> Categories <br/> ![](src/assets/imgs/capture-categories.jpg)
- Criterio -> Glasses <br/> ![](src/assets/imgs/capture-glasses.jpg)
- Criterio -> Ingredients <br/> ![](src/assets/imgs/capture-ingredients.jpg)
- Criterio -> Alcoholic <br/> ![](src/assets/imgs/capture-alcoholic-autocomplete.jpg) <br/> ![](src/assets/imgs/capture-alcoholic.jpg)
- Información de una bebida <br/> ![](src/assets/imgs/capture-selected-drink.jpg) <br/> ![](src/assets/imgs/capture-info-drink.jpg)
